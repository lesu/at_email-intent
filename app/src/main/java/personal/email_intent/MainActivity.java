package personal.email_intent;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * <ol type="I">
 * <li><a href="http://developer.android.com/reference/android/content/Intent.html#setType%28java.lang.String%29">intent.setType(String type)</a>
 * <li><a href="http://www.freeformatter.com/mime-types-list.html">Complete MIME Types List - FreeFormatter.com</a>
 * <li><a href="http://stackoverflow.com/questions/2197741/how-can-i-send-emails-from-my-android-application">Stackoverflow.com | How can I send emails from my Android application?</a>
 * </ol>
 */
public class MainActivity extends AppCompatActivity {
    private EditText editTextEmail;
    private Button buttonEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }



    private void initComponents() {
        this.editTextEmail = (EditText) this.findViewById(R.id.edit_text_email);
        this.buttonEmail = (Button) this.findViewById(R.id.button_email);

        this.buttonEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = MainActivity.this.editTextEmail.getText().toString();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                MainActivity.this.startActivity(Intent.createChooser(intent,"Email"));
            }
        });
    }

}
